import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { map } from 'rxjs/operator/map';

const API_BASE = "http://www.omdbapi.com/?";
const API_KEY = "8f44b101";

@Injectable()
export class OmdbService {

    private movie: Movie;

    constructor(private httpClient: HttpClient) {
    }

    public searchMovieByTitle(title: string): Observable<any> {
        return this.httpClient.get(API_BASE + "t=" + title + "&apikey=" + API_KEY).map((result: Response) => {
            return result;
        },
        (err: HttpErrorResponse) => {
             
        });
    }
}