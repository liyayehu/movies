import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Subscriber } from 'rxjs/Subscriber';
import { Subscription } from 'rxjs/Subscription';

import { OmdbService } from '../../providers/omdbService';
import { Movie } from '../../models/movie';
import * as fromRoot from '../../reducers/index';
import * as fromSearchMovie from '../../reducers/search-movie';
import * as fromSearchMovieActions from '../../actions/search-movie';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {

  public searchTitle: string;
  public movie: Movie;
  public searchMovieSubscription: Subscription;
  public searchMovieResultsSubscription: Subscription;
  public searchMovieState: fromSearchMovie.State;

  constructor(private store: Store<fromRoot.AppState>, private omdbService: OmdbService) {

    this.searchMovieSubscription = this.store.select(fromRoot.getSearchMovieState).subscribe((val: fromSearchMovie.State) => {
      this.searchMovieState = val;
    });

    this.searchTitle = "";
    this.movie = {
      title: "",
      year: "",
      released: "",
      duration: "",
      genre: "",
      actors: "",
      plot: "",
      language: "",
      poster: "",
      imdbRating: ""
    }
  }

  ngOnInit() {
  }


  public setSearchString(title: string) {
    this.searchTitle = title;
  }

  public searchMovie() {
    this.omdbService.searchMovieByTitle(this.searchTitle).subscribe(result => {

      this.movie = {
        title: result.Title,
        year: result.Year,
        released: result.Released,
        duration: result.Duration,
        genre: result.Genre,
        actors: result.Actors,
        plot: result.Plot,
        language: result.Language,
        poster: result.Poster,
        imdbRating: result.imdbRating,
      };

      this.store.dispatch(new fromSearchMovieActions.TriggerSearchComplete(this.movie));

    }, error => {

      this.store.dispatch(new fromSearchMovieActions.TriggerSearchFailed);

    });
  }
}
