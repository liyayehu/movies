import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MovieSearchComponent } from './containers/movie-search/movie-search.component';
import { MovieListComponent } from './containers/movie-list/movie-list.component';
import { ViewMovieComponent } from './components/view-movie/view-movie.component';
import { MaterialModule } from './material';
import { OmdbService } from './providers/omdbService';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from "./reducers"
import { EffectsModule } from '@ngrx/effects';
import { MovieSearchEffects } from './effects/movieSearchEffects';


@NgModule({
  declarations: [
    MovieSearchComponent,
    MovieListComponent,
    ViewMovieComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature('searchMovie', fromRoot.reducers),
    EffectsModule.forRoot([
      MovieSearchEffects
    ])
  ],
  entryComponents: [
    MovieSearchComponent,
    MovieListComponent,
    ViewMovieComponent
  ],
  providers: [OmdbService, HttpClientModule, HttpClient],
  bootstrap: [MovieSearchComponent]
})
export class AppModule { }
