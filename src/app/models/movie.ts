export interface Movie {
    title: string,
    year: string,
    released: string,
    duration: string,
    genre: string,
    actors: string,
    plot: string,
    language: string,
    poster: string,
    imdbRating: string
}