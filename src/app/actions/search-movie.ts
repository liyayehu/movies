import { Action } from "@ngrx/store/";
import { Movie } from "../models/movie";

export const SET_SEARCH_STRING = "[Search Movie] SET_SEARCH_STRING";
export const TRIGGER_SEARCH_COMPLETE = "[Search Movie] TRIGGER_SEARCH_COMPLETE";
export const TRIGGER_SEARCH_FAILED = "[Search Movie] TRIGGER_SEARCH_FAILED";
export const TRIGGER_MOVIE_SEARCH = "[Search Movie] TRIGGER_MOVIE_SEARCH";

export class SetSearchString implements Action {
    public readonly type = SET_SEARCH_STRING;
    public payload: string;

    constructor (payload: string) {
        this.payload = payload;
    }
}

export class TriggerSearchComplete implements Action {
    public readonly type = TRIGGER_SEARCH_COMPLETE;
    public payload: Movie;

    constructor (payload: Movie) {
        this.payload = payload;
    }
}

export class TriggerSearchFailed implements Action {
    public readonly type = TRIGGER_SEARCH_FAILED;
}

export class TriggerMovieSearch implements Action {
    public readonly type = TRIGGER_MOVIE_SEARCH;
    public payload: string;

    constructor (payload: string) {
        this.payload = payload;
    }
}

export type Actions = SetSearchString
                    | TriggerSearchComplete
                    | TriggerSearchFailed
                    | TriggerMovieSearch;