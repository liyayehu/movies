import { Injectable } from "@angular/core";
import { Observer } from "rxjs/Observer";
import { Observable } from "rxjs/Observable";
import { Action, Store } from "@ngrx/store";
import { Effect, Actions } from "@ngrx/effects";
import 'rxjs/add/operator/withLatestFrom';

import { OmdbService } from "../providers/omdbService";
import * as fromSearchMovieActions from "../actions/search-movie";
import * as fromRoot from "../reducers/index";
import { Movie } from "../models/movie";

@Injectable()
export class MovieSearchEffects {

  constructor(private store: Store<fromRoot.AppState>, private actions$: Actions, private omdbService: OmdbService) {}

  @Effect()
  search$ = this.actions$
  .ofType(fromSearchMovieActions.TRIGGER_MOVIE_SEARCH)
  .withLatestFrom(this.store)
  .map(([action, state]) => {
    // this.omdbService.searchMovieByTitle(state.searchMovie.searchString).subscribe(result => {

    //   let movie: Movie = {
    //     title: result.Title,
    //     year: result.Year,
    //     released: result.Released,
    //     duration: result.Duration,
    //     genre: result.Genre,
    //     actors: result.Actors,
    //     plot: result.Plot,
    //     language: result.Language,
    //     poster: result.Poster,
    //     imdbRating: result.imdbRating,
    //   };

    //   this.store.dispatch(new fromSearchMovieActions.TriggerSearchComplete(movie));

    // }, error => {

    //   this.store.dispatch(new fromSearchMovieActions.TriggerSearchFailed);

    // });
  })

}