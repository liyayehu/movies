import * as fromSearchMovie from "../actions/search-movie";
import { Movie } from "../models/movie";

export interface State {
    searchString: string,
    searching: boolean,
    movieResults: Movie
}

export const initialState: State = {
    searchString: "",
    searching: false,
    movieResults: {
        title: "",
        year: "",
        released: "",
        duration: "",
        genre: "",
        actors: "",
        plot: "",
        language: "",
        poster: "",
        imdbRating: ""
      }
}

export function reducer(state = initialState, action: fromSearchMovie.Actions) {
    let newState = {...state};
    switch (action.type) {
        case fromSearchMovie.SET_SEARCH_STRING:
            newState.searchString = action.payload;
            return newState;
        case fromSearchMovie.TRIGGER_MOVIE_SEARCH:
            newState.searchString = action.payload;
            newState.searching = true;
            return newState;
        case fromSearchMovie.TRIGGER_SEARCH_COMPLETE:
            newState.searching = false;
            newState.movieResults = action.payload;
            return newState;
        case fromSearchMovie.TRIGGER_SEARCH_FAILED:
            newState.searching = false;
            return newState;
        default:
            return state;
    }
}

export const getSearchMovieSearchString = (state: State) => state.searchString;
export const getSearchMovieResults = (state: State) => state.movieResults;