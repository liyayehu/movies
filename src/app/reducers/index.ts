import * as fromSearchMovie from "./search-movie";
import { ActionReducerMap, createFeatureSelector, createSelector } from "@ngrx/store";

export interface AppState {
    searchMovie: fromSearchMovie.State;
};

export const reducers: ActionReducerMap<AppState> = {
    searchMovie: fromSearchMovie.reducer,
};

export const getSearchMovieState = (state: AppState) => state.searchMovie;  

// export const getSearchMovieState = createFeatureSelector<fromSearchMovie.State>('searchMovie');
// export const getSearchMovieSearchString = createSelector(getSearchMovieState, fromSearchMovie.getSearchMovieSearchString);
// export const getSearchMovieResults = createSelector(getSearchMovieState, fromSearchMovie.getSearchMovieResults);